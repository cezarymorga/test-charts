#!/bin/bash

rancher context switch test-deployment

echo "Deploying to Rancher"
rancher catalog refresh cmorga
rancher app install --no-prompt --namespace "cmorga-ns" cattle-global-data:cmorga-cmorga "cmorga" 
rancher app show-notes cmorga
rancher app show-app cmorga
